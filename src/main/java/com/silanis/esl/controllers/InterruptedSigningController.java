package com.silanis.esl.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//Displays an "Interrupted Signing Page" for packages with statuses other than PACKAGE_COMPLETE and SIGNER_COMPLETE
@Controller
public class InterruptedSigningController {
	private static final long	serialVersionUID	= 1L;

	@RequestMapping(value="/InterruptedSigning", method=RequestMethod.GET)
	protected String doGet() {
		return "InterruptedSigning";
	}
}