package com.silanis.esl.controllers;

import java.io.IOException;

import javax.servlet.ServletException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

//Displays the "Congratulations Page" of the insurance company web application 
@Controller
public class CongratulationsController {

	private static final long	serialVersionUID	= 1L;

	@RequestMapping(value="/Congratulations", method= RequestMethod.GET)
	public String congratulation() throws ServletException, IOException {
		return "Congratulations";
	}
}