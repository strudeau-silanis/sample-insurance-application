package com.silanis.esl.services;

import org.springframework.stereotype.Service;

import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.PackageId;

//Downloads the evidence summary pdf document
@Service
public class EvidenceService {
	
	public byte[] downloadEvidenceSummary(String packageId, String API_KEY, String API_URL) {
		EslClient esl = new EslClient(API_KEY, API_URL);

		PackageId pckId = new PackageId(packageId);
		byte[] evidenceContent = esl.downloadEvidenceSummary(pckId);
		return evidenceContent;

	}
}