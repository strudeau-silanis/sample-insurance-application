package com.silanis.esl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.silanis.esl.services.FieldValuesService;

// Displays the values found in the fields of signed and completed documents
@Controller
@SessionAttributes(value={"eslApiKey", "eslApiUrl"})
public class FieldValuesController {
	private static final long serialVersionUID = 1L;
	
	@Autowired
	private FieldValuesService svc;
	
	@RequestMapping(value="/getFieldValues", method=RequestMethod.GET, produces="text/plain")
	@ResponseBody
	public String getFieldValues(@RequestParam("packageId") String packageId,
 			 				     @ModelAttribute("eslApiKey") String eslApiKey,
 			 				     @ModelAttribute("eslApiUrl") String eslApiUrl) {
		
		String fieldValues= svc.getFieldValues(packageId, eslApiKey, eslApiUrl);
		return fieldValues; 
	}
}
