package com.silanis.esl.controllers;

import java.util.concurrent.ConcurrentMap;

import javax.servlet.http.HttpServlet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.silanis.esl.Database.NotificationsDatabase;
import com.silanis.esl.models.ESLNotificationsModel;
import com.silanis.esl.services.ESLNotificationsService;


//Displays the e-SignLive event notifications
//Displays the status of the packages
@Controller
public class ESLNotificationsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final Logger log = LoggerFactory.getLogger(ESLNotificationsController.class);
	
	@Autowired
	private ESLNotificationsService svc;
	
	@Autowired
	private NotificationsDatabase database;
  
	@RequestMapping(value="/ESLNotifications", method=RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void postNotifications(@RequestBody ESLNotificationsModel eslNotification) {
		log.debug("Notification name: " + eslNotification.getName()  + "for package ID: " + eslNotification.getPackageId());
		svc.saveToDB(eslNotification);
	}
	
	@RequestMapping(value="/ESLNotifications", method=RequestMethod.GET)
	public String getNotification(Model model) {
		ConcurrentMap<String, String> myMap = database.getMap();
		model.addAttribute("myMap", myMap);
		
		return "ESLNotifications";
	}
}
