package com.silanis.esl.services;

import static com.silanis.esl.sdk.builder.CeremonyLayoutSettingsBuilder.newCeremonyLayoutSettings;
import static com.silanis.esl.sdk.builder.DocumentBuilder.newDocumentWithName;
import static com.silanis.esl.sdk.builder.DocumentPackageSettingsBuilder.newDocumentPackageSettings;
import static com.silanis.esl.sdk.builder.FieldBuilder.checkBox;
import static com.silanis.esl.sdk.builder.FieldBuilder.signatureDate;
import static com.silanis.esl.sdk.builder.FieldBuilder.textField;
import static com.silanis.esl.sdk.builder.PackageBuilder.newPackageNamed;
import static com.silanis.esl.sdk.builder.SignatureBuilder.signatureFor;
import static com.silanis.esl.sdk.builder.SignerBuilder.newSignerWithEmail;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.silanis.esl.models.InsuranceFormModel;
import com.silanis.esl.sdk.DocumentPackage;
import com.silanis.esl.sdk.DocumentType;
import com.silanis.esl.sdk.EslClient;
import com.silanis.esl.sdk.PackageId;
import com.silanis.esl.sdk.SessionToken;


//Package creation with e-SignLive
public class ESLPackageCreation {
	private static final Logger log = LoggerFactory.getLogger(ESLPackageCreation.class);
	private static final String DATA_FORMAT = "EEE, d MMM yyyy HH:mm:ss Z";
	

	public static PackageId createPackage(InsuranceFormModel model, String API_KEY, String API_URL, String AutoSubmitURL) {
		log.info("Creating new eSL package...");
		
		EslClient eslClient = new EslClient(API_KEY, API_URL);

		if (AutoSubmitURL == null || AutoSubmitURL.isEmpty()) {
			throw new RuntimeException(
					"in order to be redirected to the congratulations page a valid AutoSubmitURL needs to be defined. Please enter a URL on the welcome page.");
		}

		InputStream pdfFile = ESLPackageCreation.class
				.getResourceAsStream("/InsuranceCompanyContract.pdf");

		SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
		String customId = model.getInsuredInitials();
		String emailId = model.getEmailAddress();
		String firstName = model.getFirstName();
		String lastName = model.getLastName();
		String address = model.getAddress();
		String city = model.getCity();
		String province = model.getProvince();
		String vehiclemodel = model.getVehicleModel();
		String vehiclemake = model.getVehicleMake();
		String vehiclemodelyear = model.getVehicleModelYear();
		String vehiclecolor = model.getVehicleColor();

		// Build the DocumentPackage object
		DocumentPackage documentPackage = newPackageNamed("Policy " + format.format(new Date()))

				// Customizing setting
				.withSettings(newDocumentPackageSettings()
								.withDecline()
								.withOptOut()
								.withDocumentToolbarDownloadButton()
								.withDialogOnComplete()
								.withHandOverLinkHref(AutoSubmitURL)
								//TODO: Customize the HandOverLinkText and HandOverLinkTooltip
								
								
								//TODO: Enable in-person signing.
								 

								// Customizing Layout
								.withCeremonyLayoutSettings(newCeremonyLayoutSettings()
												.withoutGlobalNavigation()
												.withoutBreadCrumbs()
												.withoutSessionBar()
												.withProgressBar()
												.withTitle()))

				// Define the insured first and last name
				.withSigner(newSignerWithEmail(emailId).withCustomId("signer1")
								.withFirstName(firstName)
								.withLastName(lastName))
				//TODO: Add an additional signer.
				// Define the document
				.withDocument(newDocumentWithName("InsuranceForm")
								.fromStream(pdfFile, DocumentType.PDF)
								.enableExtraction()
								.withSignature(signatureFor(emailId)
												.withName("InsuredSignature")
												.withPositionExtracted()

												// Bound fields
												//TODO: Add a textField with name "ExtraInfo"

												.withField(signatureDate()
																.withPositionExtracted()
																.withName("Date"))

												.withField(checkBox()
																.withPositionExtracted()
																.withName("checkbox"))

								)
								//TODO: Add another signature using x-y coordinates for your new signer.

								// Below are the form fields filled in by the Insured/customer

								.withInjectedField(textField().withName("CustomerId")
												.withValue(customId))

								.withInjectedField(textField().withName("FirstName")
												.withValue(firstName))

								.withInjectedField(textField().withName("LastName")
												.withValue(lastName))
								.withInjectedField(textField().withName("Address")
												.withValue(address))
								.withInjectedField(textField().withName("City").withValue(
												city))

								.withInjectedField(textField().withName("Province")
												.withValue(province))

								.withInjectedField(textField().withName("VehicleModel")
												.withValue(vehiclemodel))

								.withInjectedField(textField().withName("VehicleMake")
												.withValue(vehiclemake))

								.withInjectedField(textField()
												.withName("VehicleModelYear")
												.withValue(vehiclemodelyear))

								//TODO: merge injected field "VehicleColor"

				).build();

		// Issue the request to the e-SignLive server to create the DocumentPackage
		PackageId packageId = eslClient.createPackage(documentPackage);
		eslClient.sendPackage(packageId);
		return packageId;

	}

	public static SessionToken createSessionToken(PackageId packageId,String customId, String API_KEY, String API_URL) {
		EslClient eslClient = new EslClient(API_KEY, API_URL);
		
		String pkgId = packageId.toString();

		SessionToken sessionToken = eslClient.getSessionService()
				.createSessionToken(pkgId, customId);
		log.info("session token: {}", sessionToken.getSessionToken());
				return sessionToken;
	}
}
