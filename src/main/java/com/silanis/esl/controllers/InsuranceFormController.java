package com.silanis.esl.controllers;

import javax.servlet.http.HttpServlet;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.silanis.esl.models.InsuranceFormModel;
import com.silanis.esl.sdk.PackageId;
import com.silanis.esl.sdk.SessionToken;
import com.silanis.esl.services.ESLPackageCreation;

//Displays the "Insurance Form Page" of the insurance company web application
@Controller
@SessionAttributes(value={"eslApiKey", "eslApiUrl", "autoSubmitUrl"})
public class InsuranceFormController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@RequestMapping(value="/InsuranceForm", method=RequestMethod.GET)
	public String doInsuranceForm() {
		return "InsuranceForm";
	}

	@RequestMapping(value="/InsuranceForm", method=RequestMethod.POST)
	public String doPost(InsuranceFormModel form, 
					     Model model,
						 @ModelAttribute("eslApiKey") String eslApiKey,
					     @ModelAttribute("eslApiUrl") String eslApiUrl,
					     @ModelAttribute("autoSubmitUrl") String autoSubmitUrl) {
				
		// Create a packageId    
		PackageId packageId = ESLPackageCreation.createPackage(form, eslApiKey, eslApiUrl, autoSubmitUrl);
		
		//Get the session token 
		SessionToken token = ESLPackageCreation.createSessionToken(packageId,"signer1", eslApiKey, eslApiUrl);
		model.addAttribute("token", token);
		
	    return "ESLSignature";
	}
	
}