package com.silanis.esl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.silanis.esl.services.DownloadDocumentsService;

//Displays the signed documents in a zip file for view or download

@Controller
@SessionAttributes(value={"eslApiKey", "eslApiUrl"})
public class DownloadDocumentsController {

	private static final long	serialVersionUID	= 1L;
	
	@Autowired
	private DownloadDocumentsService svc;

	@RequestMapping(value="/downloadDocuments", method=RequestMethod.GET, produces="application/zip")
	protected ResponseEntity<byte[]> getDocument(@RequestParam("packageId") String packageId,
							     @ModelAttribute("eslApiKey") String eslApiKey,
							     @ModelAttribute("eslApiUrl") String eslApiUrl) {
		
		byte[] downloadDocs = svc.downloadZippedDocuments(packageId, eslApiKey, eslApiUrl);
		return ResponseEntity.ok(downloadDocs);
	}

}
