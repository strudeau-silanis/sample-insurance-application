package com.silanis.esl.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.silanis.esl.services.EvidenceService;

//Displays the Evidence Summary in the form of a PDF file.
@Controller
@SessionAttributes(value={"eslApiKey", "eslApiUrl"})
public class EvidenceController {

	private static final long	serialVersionUID	= 1L;
	
	@Autowired
	private EvidenceService svc;

	@RequestMapping(value="/downloadEvidenceSummary", method=RequestMethod.GET, produces="application/pdf")
	public ResponseEntity<byte[]> getEvidenceSummary(@RequestParam("packageId") String packageId,
		     			 			 @ModelAttribute("eslApiKey") String eslApiKey,
		     			 			 @ModelAttribute("eslApiUrl") String eslApiUrl) {
				
		byte[] evidenceSummary = svc.downloadEvidenceSummary(packageId, eslApiKey, eslApiUrl);
		return ResponseEntity.ok(evidenceSummary);
	}

}
