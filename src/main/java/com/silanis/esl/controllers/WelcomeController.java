package com.silanis.esl.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

// Displays the "Welcome Page" of the insurance company web application where the user is promoted to start filling his insurance form 

@Controller
@SessionAttributes(value={"eslApiKey", "eslApiUrl", "autoSubmitUrl"})
public class WelcomeController {

	@RequestMapping(value={"/", "/index"}, method= RequestMethod.GET)
	public String welcome() {
		return "Welcome";
	}
	
	@RequestMapping(value={"/", "/index"}, method= RequestMethod.POST)
	public String postEslCredentials(Model model,
						  			 @RequestParam("eslApiKey") String eslApiKey,
						  			 @RequestParam("eslApiUrl") String eslApiUrl,
						  			 @RequestParam("autoSubmitUrl") String autoSubmitUrl) {
		model.addAttribute("eslApiKey", eslApiKey);
		model.addAttribute("eslApiUrl", eslApiUrl);
		model.addAttribute("autoSubmitUrl", autoSubmitUrl);
		
		return "InsuranceForm";		
	}
}
