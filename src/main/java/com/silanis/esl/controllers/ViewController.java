package com.silanis.esl.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.silanis.esl.models.NotificationEventsType;

//Displays a page indicating successful or incomplete signing depending on the package status
@Controller
public class ViewController {
	private static final long	serialVersionUID	= 1L;
	private static String		Success				= "AutoSubmitOnComplete";
	private static String		Incomplete			= "AutoSubmitOnOther";

	@RequestMapping(value="/AutoSubmit", method=RequestMethod.GET)
	public String doGet(@RequestParam("status") String notificationName) {
		String forward = "";
		
		if (notificationName.contentEquals(NotificationEventsType.PACKAGE_COMPLETE.toString())
				|| notificationName.contentEquals(NotificationEventsType.SIGNER_COMPLETE.toString())) {
			forward = Success;
		}
		else {
			forward = Incomplete;
		}

		return forward;
	}
}
