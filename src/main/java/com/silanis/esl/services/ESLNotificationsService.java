package com.silanis.esl.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.silanis.esl.Database.NotificationsDatabase;
import com.silanis.esl.models.ESLNotificationsModel;
import com.silanis.esl.models.NotificationEventsType;


// Registers and saves the event notifications
@Service
public class ESLNotificationsService {
	private static final Logger log = LoggerFactory.getLogger(ESLPackageCreation.class);

	public void saveToDB(ESLNotificationsModel eslNotification) {
		NotificationsDatabase database = NotificationsDatabase.getInstance();
		String notificationName = eslNotification.getName();
		String notificationPackageId = eslNotification.getPackageId();
				
		log.info("Notification listener is up... ");
		log.info(notificationName + " received..");
		
		//Package has been sent to be signed
        if(notificationName.contentEquals(NotificationEventsType.PACKAGE_ACTIVATE.toString())){
        	database.save(notificationPackageId,notificationName );
       	}
        
       //Package has been signed and confirmed by all signers, and marked as complete 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_COMPLETE.toString())){
           	database.save(notificationPackageId,notificationName );
        }
 
        //Package has passed its expiry date and been deactivated 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_EXPIRE.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
       //Package has deactivated because one of the signers has opted out of the electronic signing process 
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_OPT_OUT.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
    	//Package has deactivated because one of the signers does not want to sign
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_DECLINE.toString())){
           	database.save(notificationPackageId,notificationName );
        }
        
      	//Signer has completed signing all documents 
        else if(notificationName.contentEquals(NotificationEventsType.SIGNER_COMPLETE.toString())){
        	database.save(notificationPackageId,notificationName );
        }

      	//Document has been signed by one of the signers 
        else if(notificationName.contentEquals(NotificationEventsType.DOCUMENT_SIGNED.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
     	//A signer has reassigned their signing responsibilities to a new signer
        else if(notificationName.contentEquals(NotificationEventsType.ROLE_REASSIGN.toString())){
        	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been created
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_CREATE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been removed from the Trash folder
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_DELETE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been trashed
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_TRASH.toString())){
              	database.save(notificationPackageId,notificationName );
        }
                
        //Package activate email bounce has occured
        else if(notificationName.contentEquals(NotificationEventsType.EMAIL_BOUNCE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A signer has failed KBA authentication
        else if(notificationName.contentEquals(NotificationEventsType.KBA_FAILURE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A signer has been locked
        else if(notificationName.contentEquals(NotificationEventsType.SIGNER_LOCKED.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //An attachment has been uploaded to the package
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_ATTACHMENT.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been deactivated
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_DEACTIVATE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A package is ready for completion
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_READY_FOR_COMPLETE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        //A package has been restored
        else if(notificationName.contentEquals(NotificationEventsType.PACKAGE_RESTORE.toString())){
              	database.save(notificationPackageId,notificationName );
        }
        
        else {
        	throw new RuntimeException("Unhandled eSL notification: "+notificationName);
        }
       
	}
}



